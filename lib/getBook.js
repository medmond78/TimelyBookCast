var request = require("request");
var castBook = require('./castBook');


function searchAndCast(){
	
 //Get the book details from PBS
var options = { method: 'GET',
  url: 'http://www.paperbackswap.com/api/v2/index.php',
  qs: { RequestType: 'RecentlyPosted', Limit: '1' },
  headers: 
   { 'postman-token': '6231a8b4-9d21-4b2e-1dbe-c11677b78c0c',
     'cache-control': 'no-cache',
     'content-type': 'application/json' },
  json: true };

 
request(options, function (error, response, body) {
  if (error) throw new Error(error);

  // Assign URLs as variables and pass to getImage...
  
  details_url = body.Response.Books.Book.BookDetailsLink;
  image_url = body.Response.Books.Book.CoverImages.LargeImage;
  book_title = body.Response.Books.Book.Title;
  console.log(body.Response.Books.Book.Title);
  console.log(body.Response.Books.Book.Authors.Author);
  book_author = body.Response.Books.Book.Authors.Author;
  console.log(book_title);
  
  
  
  castBook(image_url,details_url,book_title,book_author);
});

};

module.exports = searchAndCast
