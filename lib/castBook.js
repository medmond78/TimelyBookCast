
var moment = require('moment');
var request = require("request");
var castImage = require('./castImage');

//Load Whenhub access token key with try/catch
var fileName = "./credentials.json";
var config;

try {
	config = require(fileName);
} catch (err) {
	config = {}
	console.log("unable to read file '" + fileName + "': ", err)
}


console.log(moment().format());

function castBook(image_url,details_url,book_title,book_author) {

// Post the Book to WhenCast :)

  console.log(book_title);
  
var options = { method: 'POST',
  url: 'https://api.whenhub.com/api/schedules/5903f2abc832b42b945df6ac/events',
  qs: { access_token: config.token },
  headers: 
   { 'postman-token': 'dac36140-79d9-1f4d-ff35-edb29839c3fd',
     'cache-control': 'no-cache',
     'content-type': 'application/json' },
  body: 
   { when: 
      { period: 'minute',
        startDate: moment().format(), //'2017-04-19T23:00:00Z',
        startTimezone: 'America/New_York',
        endDate: null,
        endTimezone: 'America/New_York' },
     primaryAction: {
    label: 'Order this book now!',
    url: details_url, //'https://www.cnn.com',
	},
	name: book_title,
	description: book_author,
	url: details_url,
    scheduleId: '5903f2abc832b42b945df6ac' },

	 
	 
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
  event_id = body.id;
  console.log(event_id);
  // Grab the returned event ID as event_id
  
  // Add image
  // This is where the call to postImage will go
  // 
  castImage(image_url,event_id);
  
});

};



module.exports = castBook