var request = require("request");


function castImage(image_url,event_id) {

console.log(event_id);

//Load Whenhub access token key with try/catch
var fileName = "./credentials.json";
var config;

try {
	config = require(fileName);
} catch (err) {
	config = {}
	console.log("unable to read file '" + fileName + "': ", err)
}


//Create the POST package for Whenhub

var options = { method: 'POST',
  url: 'https://api.whenhub.com/api/events/' + event_id + '/media',
  qs: { access_token: config.token },
  headers: 
   { 'postman-token': 'dac36140-79d9-1f4d-ff35-edb29839c3fd',
     'cache-control': 'no-cache',
     'content-type': 'application/json' },

	 
  body: 
   { 
  "type": "image", // "image"
  "url": image_url,
  "order": 0,
  "autoScaled": true
  },
  json: true };

//Send the POST request to Whenhub
request(options, function (error, response, body) {
  if (error) throw new Error(error);
  console.log(body);

});

};




module.exports = castImage