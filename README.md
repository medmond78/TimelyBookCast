# What I am looking at?
This is the codebase that drives the TimelyBookCast visible here https://studio.whenhub.com/schedules/5903f2abc832b42b945df6ac/timelybookcast

## What is TimelyBookCast?
The TimelyBookCast app is a service that polls the PaperBackSwap (PBS) book trading site hourly. The latest book posted for trading is pushed to the Whencast as a new event. The book cover is used as the event image, and a call-to-action (CTA) button allows the user to visit the book page directly to order.

## What the heck is PBS?
PBS is a service that allows users to find and obtain mostly older, hard to find books by trading books they already own with fellow users. Each book sent out by a user (to another member) earns them a credit. That credit can then be used in turn to request another book. Users can also buy credits or earn credits through a paid membership. But more importantly to us, PBS also offers an API which allows developers to search, access user bookshelves, and other tasks.

## How does it work?
Every hour, the following workflow kicks off:  The `getBook.js` file fires a GET request to the PBS API and retrieves a JSON package with the latest book posted for trading. Along with the use of `moment.js` module to get a timestamp, the details are passed to `castBook.js`, which posts the book details to the Whenhub schedule as an event using the Whenhub API. A subsequent call using `castImage.js` sends the book cover image to the Whenhub API as a media attachment for the newly-created event.

## Where does it run?
The app runs on AWS as a timed Lambda function. This is very time and cost-efficient means of running the app, as it consumes minimal computational time (being run every hour). Lambda avoids us having to pay for idle cycles on a standard AWS server.


